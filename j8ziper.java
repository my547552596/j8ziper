import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.*;

public class j8ziper {
  private static final String sVersion = "j8ziper v1.0.1 - m@20190507.yiyi";

  public static void main(String[] sa) throws Exception {
    if(sa.length > 0) {
      for(String s : sa) {
        zipack(new File(s));
      }
    }
    System.out.println(sVersion);
    System.exit(0);
  }

  private static void zipack(File f) throws Exception {
    System.out.println(f.getName() + " (");
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    FileOutputStream fos = new FileOutputStream(f.getName() + " " + sdf.format(new Date()) + "a2.zip");
    CheckedOutputStream cos = new CheckedOutputStream(fos, new CRC32());

    ZipOutputStream zos = new ZipOutputStream(cos);
    zos.setComment(sVersion);
    zos.setLevel(Deflater.BEST_COMPRESSION);
    zos.setMethod(ZipOutputStream.DEFLATED);
    zipack(f, f.isDirectory() ? "" : f.getName(), zos);
    zos.close();

    cos.close();
    fos.close();
    System.out.println(")\r\n");
  }

  private static void zipack(File f, String s, ZipOutputStream zos) throws Exception {
    if(f.isDirectory()) {
      s += s.equals("") ? "" : File.separator;

      for(File F : f.listFiles()) {
        zipack(F, s + F.getName(), zos);
      }
    }

    if(f.isFile()) {
      zos.putNextEntry(new ZipEntry(s));
      System.out.println(s);

      Long l = f.length();
      byte[] b = new byte[l.intValue()];
      FileInputStream fis = new FileInputStream(f);
      fis.read(b);
      fis.close();

      zos.write(b);
      zos.flush();
      zos.closeEntry();
    }
  }
}